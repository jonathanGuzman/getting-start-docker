# getting-start-docker

Simple bash to install docker, docker-compose and run gitlab runner

## Install
Run the following command lines:

```
//install docker
sudo sh install-docker.sh
//install docker-compose
sudo sh install-docker-compose.sh
//run gitlab runner
sudo sh install-gitlab-runner.sh
```
